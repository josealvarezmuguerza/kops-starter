# not really needed DEFAULT profile will be used instead
# export AWS_ACCESS_KEY_ID=
# export AWS_SECRET_ACCESS_KEY=
# el nombre del cluster tiene que terminar con .local para no generar entradas en route53
export NAME=<nombre>.k8s.local
# a bucket for storing configuration
export KOPS_STATE_STORE=s3://
# aws region ei: us-east-1
export KOPS_ZONES=
export KOPS_NODE_COUNT=1
export KOPS_NODE_SIZE=t3.medium
export KOPS_MASTER_SIZE=t3.medium

kops create cluster --name=${NAME} --zones=${KOPS_ZONES} --node-count=${KOPS_NODE_COUNT} --node-size=${KOPS_NODE_SIZE} --master-size=${KOPS_MASTER_SIZE}

echo -n "Editar el cluster ${NAME}? [N/y]: " 
read -r a

if [ "$a" == "y" ]
then
    kops edit cluster ${NAME}
fi

echo -n "Editar los nodos? [N/y]: "
read -r a

if [ "$a" == "y" ]
then
    kops edit ig --name=${NAME} nodes
fi

echo -n "Editar el master? [N/y]: "
read -r a

if [ "$a" == "y" ]
then
    kops edit ig --name=${NAME} master-${KOPS_ZONES}
fi

echo -n "Crear el cluster ${NAME}? [Y/n]: "
read -r a

if [ "$a" != "n" ]
then
    kops update cluster --name ${NAME} --yes

fi

echo -n "Borrar el cluster ${NAME}? [Y/n]: "
read -r a

if [ "$a" != "n" ]
then
    kops delete cluster --name=${NAME} --yes
fi
